#nodejs-lite-logger
[![npm version](https://badge.fury.io/js/nodejs-lite-logger.svg)](https://badge.fury.io/js/nodejs-lite-logger)

Lightweight logger for colorize console in Nodejs


####Usage
```javascript 1.8
const { info, success, warning, error } = require('nodejs-lite-logger');

info('test info');
success('test success');
warning('test warning');
error('test error');
```
####Example in white console
![Alt Text](https://gitlab.com/Oleg.samoylov/lite-logger/raw/master/assets/logger1.png)

####Example in dark console
![Alt Text](https://gitlab.com/Oleg.samoylov/lite-logger/raw/master/assets/logger2.png)

I am wait your proposals for improvement :)