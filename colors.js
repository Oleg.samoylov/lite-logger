module.exports = {
  "error": "\x1b[31m%s\x1b[0m",
  "success": "\x1b[32m%s\x1b[0m",
  "info": "\x1b[34m%s\x1b[0m",
  "warning": "\x1b[33m%s\x1b[0m"
};
