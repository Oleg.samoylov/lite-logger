const colors = require('./colors');
const logger = {};
Object.keys(colors).forEach((key)=>{
  Object.defineProperty(logger, key, {
    writable: false,
    configurable: false,
    enumerable: true,
    value: (...args)=> console.log(
      colors[key],
      `${key.toUpperCase()} [${new Date().toISOString()}] : `,
      ...args
    ),
  })
});

module.exports = logger;